﻿#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This experiment was created using PsychoPy3 Experiment Builder (v2020.1.1),
    on February 24, 2020, at 10:18
If you publish work using this script the most relevant publication is:

    Peirce J, Gray JR, Simpson S, MacAskill M, Höchenberger R, Sogo H, Kastman E, Lindeløv JK. (2019) 
        PsychoPy2: Experiments in behavior made easy Behav Res 51: 195. 
        https://doi.org/10.3758/s13428-018-01193-y

"""

from __future__ import absolute_import, division

from psychopy import locale_setup
from psychopy import prefs
from psychopy import sound, gui, visual, core, data, event, logging, clock
from psychopy.constants import (NOT_STARTED, STARTED, PLAYING, PAUSED,
                                STOPPED, FINISHED, PRESSED, RELEASED, FOREVER)

import numpy as np  # whole numpy lib is available, prepend 'np.'
from numpy import (sin, cos, tan, log, log10, pi, average,
                   sqrt, std, deg2rad, rad2deg, linspace, asarray)
from numpy.random import random, randint, normal, shuffle
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard



# Ensure that relative paths start from the same directory as this script
_thisDir = os.path.dirname(os.path.abspath(__file__))
os.chdir(_thisDir)

# Store info about the experiment session
psychopyVersion = '2020.1.1'
expName = 'rds_task'  # from the Builder filename that created this script
expInfo = {'participant': '', 'session': '001', 'practice': False, 'config': 'config_TI.json', 'port': 'COM5'}
dlg = gui.DlgFromDict(dictionary=expInfo, sortKeys=False, title=expName)
if dlg.OK == False:
    core.quit()  # user pressed cancel
expInfo['date'] = data.getDateStr()  # add a simple timestamp
expInfo['expName'] = expName
expInfo['psychopyVersion'] = psychopyVersion

# Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
filename = _thisDir + os.sep + u'data/%s_%s_%s' % (expInfo['participant'], expName, expInfo['date'])

# An ExperimentHandler isn't essential but helps with data saving
thisExp = data.ExperimentHandler(name=expName, version='',
    extraInfo=expInfo, runtimeInfo=None,
    originPath='D:\\Projects\\reverse-digit-span-task\\rds_task_lastrun.py',
    savePickle=True, saveWideText=True,
    dataFileName=filename)
# save a log file for detail verbose info
logFile = logging.LogFile(filename+'.log', level=logging.EXP)
logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

endExpNow = False  # flag for 'escape' or other condition => quit the exp
frameTolerance = 0.001  # how close to onset before 'same' frame

# Start Code - component code to be run before the window creation

# Setup the Window
win = visual.Window(
    size=[1920, 1080], fullscr=True, screen=0, 
    winType='pyglet', allowGUI=False, allowStencil=False,
    monitor='testMonitor', color=[0,0,0], colorSpace='rgb',
    blendMode='avg', useFBO=True, 
    units='deg')
# store frame rate of monitor if we can measure it
expInfo['frameRate'] = win.getActualFrameRate()
if expInfo['frameRate'] != None:
    frameDur = 1.0 / round(expInfo['frameRate'])
else:
    frameDur = 1.0 / 60.0  # could not measure, so guess

# create a default keyboard (e.g. to check for escape)
defaultKeyboard = keyboard.Keyboard()

# Initialize components for Routine "Instruction"
InstructionClock = core.Clock()
instruction = visual.TextStim(win=win, name='instruction',
    text='Welcome!\n\nIn this task, you will be shown a series of numbers, and asked to recall them in REVERSE order.\n\nFor example, if you saw the number 1 then 2, you would answer 2 then 1, as this would be the original list backwards. Do not recall the numbers in the order you see them.\n\nThis task will continue for 20 trials, and you will find that the amount of numbers you have to remember changes.\n\nPlease, respond to each list by using the numbers on the keyboard. \nEven if you cannot quite remember a list, please, give it your best guess.\n\nPress SPACE to start',
    font='Calibri',
    pos=(0, 0), height=1, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
key_start = keyboard.Keyboard()
from pyniexp import stimulation

MINLEVEL=0
MAXLEVEL=9
shamAmp = [2,2]

if expInfo['practice']:
    runs = list(range(1))
    nTrial = 2
else:
    runs = list(range(3))
    nTrial = 20
    stimulator = stimulation.TI(expInfo['config'])
    stimulator.port = expInfo['port']
    stimulator.connect()
    stimulator.load()



# Initialize components for Routine "stimulation_start"
stimulation_startClock = core.Clock()

# Initialize components for Routine "fixation"
fixationClock = core.Clock()
cross = visual.TextStim(win=win, name='cross',
    text='+',
    font='Calibri',
    pos=(0, 0), height=4, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "trial"
trialClock = core.Clock()
digits = visual.TextStim(win=win, name='digits',
    text='default text',
    font='Calibri',
    pos=(0, 0), height=4, wrapWidth=30, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "response"
responseClock = core.Clock()
resp_stim = visual.TextStim(win=win, name='resp_stim',
    text='?',
    font='Calibri',
    pos=(0, 0), height=4, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);
resp_keys = keyboard.Keyboard()

# Initialize components for Routine "response_last"
response_lastClock = core.Clock()
resp_last = visual.TextStim(win=win, name='resp_last',
    text='default text',
    font='Calibri',
    pos=(0, 0), height=4, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "feedback"
feedbackClock = core.Clock()
feedback_stim = visual.TextStim(win=win, name='feedback_stim',
    text='Correct',
    font='Calibri',
    pos=(0, 0), height=4, wrapWidth=None, ori=0, 
    color='white', colorSpace='rgb', opacity=1, 
    languageStyle='LTR',
    depth=0.0);

# Initialize components for Routine "stimulation_stop"
stimulation_stopClock = core.Clock()

# Create some handy timers
globalClock = core.Clock()  # to track the time since experiment started
routineTimer = core.CountdownTimer()  # to track time remaining of each (non-slip) routine 

# set up handler to look after randomisation of conditions etc
runs = data.TrialHandler(nReps=1, method='sequential', 
    extraInfo=expInfo, originPath=-1,
    trialList=data.importConditions('conditions.xlsx', selection=runs),
    seed=None, name='runs')
thisExp.addLoop(runs)  # add the loop to the experiment
thisRun = runs.trialList[0]  # so we can initialise stimuli with some values
# abbreviate parameter names if possible (e.g. rgb = thisRun.rgb)
if thisRun != None:
    for paramName in thisRun:
        exec('{} = thisRun[paramName]'.format(paramName))

for thisRun in runs:
    currentLoop = runs
    # abbreviate parameter names if possible (e.g. rgb = thisRun.rgb)
    if thisRun != None:
        for paramName in thisRun:
            exec('{} = thisRun[paramName]'.format(paramName))
    
    # ------Prepare to start Routine "Instruction"-------
    continueRoutine = True
    # update component parameters for each repeat
    key_start.keys = []
    key_start.rt = []
    _key_start_allKeys = []
    if not(expInfo['practice']):
        stimulator.amplitude = [int(a) for a in thisRun[expInfo['participant']].split('_')]
    
    # keep track of which components have finished
    InstructionComponents = [instruction, key_start]
    for thisComponent in InstructionComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    InstructionClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "Instruction"-------
    while continueRoutine:
        # get current time
        t = InstructionClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=InstructionClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # *instruction* updates
        if instruction.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            instruction.frameNStart = frameN  # exact frame index
            instruction.tStart = t  # local t and not account for scr refresh
            instruction.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(instruction, 'tStartRefresh')  # time at next scr refresh
            instruction.setAutoDraw(True)
        if instruction.status == STARTED:
            if bool(False):
                # keep track of stop time/frame for later
                instruction.tStop = t  # not accounting for scr refresh
                instruction.frameNStop = frameN  # exact frame index
                win.timeOnFlip(instruction, 'tStopRefresh')  # time at next scr refresh
                instruction.setAutoDraw(False)
        
        # *key_start* updates
        if key_start.status == NOT_STARTED and t >= 0.0-frameTolerance:
            # keep track of start time/frame for later
            key_start.frameNStart = frameN  # exact frame index
            key_start.tStart = t  # local t and not account for scr refresh
            key_start.tStartRefresh = tThisFlipGlobal  # on global time
            win.timeOnFlip(key_start, 'tStartRefresh')  # time at next scr refresh
            key_start.status = STARTED
            # keyboard checking is just starting
            key_start.clock.reset()  # now t=0
        if key_start.status == STARTED:
            theseKeys = key_start.getKeys(keyList=['space'], waitRelease=False)
            _key_start_allKeys.extend(theseKeys)
            if len(_key_start_allKeys):
                key_start.keys = _key_start_allKeys[-1].name  # just the last key pressed
                key_start.rt = _key_start_allKeys[-1].rt
                # a response ends the routine
                continueRoutine = False
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in InstructionComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "Instruction"-------
    for thisComponent in InstructionComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "Instruction" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # ------Prepare to start Routine "stimulation_start"-------
    continueRoutine = True
    # update component parameters for each repeat
    if not(expInfo['practice']):
        if stimulator.amplitude == [0,0]:
            stimulator.amplitude = shamAmp
            stimulator.start()
            stimulator.stop()
        else:
            stimulator.start()
    # keep track of which components have finished
    stimulation_startComponents = []
    for thisComponent in stimulation_startComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    stimulation_startClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "stimulation_start"-------
    while continueRoutine:
        # get current time
        t = stimulation_startClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=stimulation_startClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in stimulation_startComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "stimulation_start"-------
    for thisComponent in stimulation_startComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "stimulation_start" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
    
    # --------Prepare to start Staircase "stair" --------
    # set up handler to look after next chosen value etc
    stair = data.StairHandler(startVal=MAXLEVEL-1, extraInfo=expInfo,
        stepSizes=[1], stepType='lin',
        nReversals=1, nTrials=nTrial, 
        nUp=1, nDown=1,
        minVal=MINLEVEL, maxVal=MAXLEVEL,
        originPath=-1, name='stair')
    thisExp.addLoop(stair)  # add the loop to the experiment
    level = thisStair = MAXLEVEL-1  # initialise some vals
    
    for thisStair in stair:
        currentLoop = stair
        level = thisStair
        
        # ------Prepare to start Routine "fixation"-------
        continueRoutine = True
        routineTimer.add(3.000000)
        # update component parameters for each repeat
        sequence=np.random.choice(range(10),size=MAXLEVEL-level+1,replace=False)
        stair.addOtherData('digits.stimulus', sequence)
        
        response_keys = []
        response_rt = []
        resp_stim.text = '?'
        # keep track of which components have finished
        fixationComponents = [cross]
        for thisComponent in fixationComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        fixationClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "fixation"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = fixationClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=fixationClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *cross* updates
            if cross.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                cross.frameNStart = frameN  # exact frame index
                cross.tStart = t  # local t and not account for scr refresh
                cross.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(cross, 'tStartRefresh')  # time at next scr refresh
                cross.setAutoDraw(True)
            if cross.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > cross.tStartRefresh + 3-frameTolerance:
                    # keep track of stop time/frame for later
                    cross.tStop = t  # not accounting for scr refresh
                    cross.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(cross, 'tStopRefresh')  # time at next scr refresh
                    cross.setAutoDraw(False)
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in fixationComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "fixation"-------
        for thisComponent in fixationComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        stair.addOtherData('cross.started', cross.tStartRefresh)
        stair.addOtherData('cross.stopped', cross.tStopRefresh)
        
        # set up handler to look after randomisation of conditions etc
        span_stim = data.TrialHandler(nReps=MAXLEVEL-level+1, method='sequential', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='span_stim')
        thisExp.addLoop(span_stim)  # add the loop to the experiment
        thisSpan_stim = span_stim.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisSpan_stim.rgb)
        if thisSpan_stim != None:
            for paramName in thisSpan_stim:
                exec('{} = thisSpan_stim[paramName]'.format(paramName))
        
        for thisSpan_stim in span_stim:
            currentLoop = span_stim
            # abbreviate parameter names if possible (e.g. rgb = thisSpan_stim.rgb)
            if thisSpan_stim != None:
                for paramName in thisSpan_stim:
                    exec('{} = thisSpan_stim[paramName]'.format(paramName))
            
            # ------Prepare to start Routine "trial"-------
            continueRoutine = True
            routineTimer.add(1.000000)
            # update component parameters for each repeat
            digits.setText(str(sequence[currentLoop.thisN]))
            # keep track of which components have finished
            trialComponents = [digits]
            for thisComponent in trialComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            trialClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
            frameN = -1
            
            # -------Run Routine "trial"-------
            while continueRoutine and routineTimer.getTime() > 0:
                # get current time
                t = trialClock.getTime()
                tThisFlip = win.getFutureFlipTime(clock=trialClock)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *digits* updates
                if digits.status == NOT_STARTED and tThisFlip >= 0-frameTolerance:
                    # keep track of start time/frame for later
                    digits.frameNStart = frameN  # exact frame index
                    digits.tStart = t  # local t and not account for scr refresh
                    digits.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(digits, 'tStartRefresh')  # time at next scr refresh
                    digits.setAutoDraw(True)
                if digits.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > digits.tStartRefresh + 1.0-frameTolerance:
                        # keep track of stop time/frame for later
                        digits.tStop = t  # not accounting for scr refresh
                        digits.frameNStop = frameN  # exact frame index
                        win.timeOnFlip(digits, 'tStopRefresh')  # time at next scr refresh
                        digits.setAutoDraw(False)
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in trialComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # -------Ending Routine "trial"-------
            for thisComponent in trialComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            span_stim.addData('digits.started', digits.tStartRefresh)
            span_stim.addData('digits.stopped', digits.tStopRefresh)
        # completed MAXLEVEL-level+1 repeats of 'span_stim'
        
        
        # set up handler to look after randomisation of conditions etc
        span_resp = data.TrialHandler(nReps=MAXLEVEL-level+1, method='sequential', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='span_resp')
        thisExp.addLoop(span_resp)  # add the loop to the experiment
        thisSpan_resp = span_resp.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisSpan_resp.rgb)
        if thisSpan_resp != None:
            for paramName in thisSpan_resp:
                exec('{} = thisSpan_resp[paramName]'.format(paramName))
        
        for thisSpan_resp in span_resp:
            currentLoop = span_resp
            # abbreviate parameter names if possible (e.g. rgb = thisSpan_resp.rgb)
            if thisSpan_resp != None:
                for paramName in thisSpan_resp:
                    exec('{} = thisSpan_resp[paramName]'.format(paramName))
            
            # ------Prepare to start Routine "response"-------
            continueRoutine = True
            # update component parameters for each repeat
            resp_keys.keys = []
            resp_keys.rt = []
            _resp_keys_allKeys = []
            # keep track of which components have finished
            responseComponents = [resp_stim, resp_keys]
            for thisComponent in responseComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            responseClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
            frameN = -1
            
            # -------Run Routine "response"-------
            while continueRoutine:
                # get current time
                t = responseClock.getTime()
                tThisFlip = win.getFutureFlipTime(clock=responseClock)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *resp_stim* updates
                if resp_stim.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                    # keep track of start time/frame for later
                    resp_stim.frameNStart = frameN  # exact frame index
                    resp_stim.tStart = t  # local t and not account for scr refresh
                    resp_stim.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(resp_stim, 'tStartRefresh')  # time at next scr refresh
                    resp_stim.setAutoDraw(True)
                
                # *resp_keys* updates
                waitOnFlip = False
                if resp_keys.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                    # keep track of start time/frame for later
                    resp_keys.frameNStart = frameN  # exact frame index
                    resp_keys.tStart = t  # local t and not account for scr refresh
                    resp_keys.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(resp_keys, 'tStartRefresh')  # time at next scr refresh
                    resp_keys.status = STARTED
                    # keyboard checking is just starting
                    waitOnFlip = True
                    win.callOnFlip(resp_keys.clock.reset)  # t=0 on next screen flip
                    win.callOnFlip(resp_keys.clearEvents, eventType='keyboard')  # clear events on next screen flip
                if resp_keys.status == STARTED and not waitOnFlip:
                    theseKeys = resp_keys.getKeys(keyList=['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'], waitRelease=False)
                    _resp_keys_allKeys.extend(theseKeys)
                    if len(_resp_keys_allKeys):
                        resp_keys.keys = _resp_keys_allKeys[0].name  # just the first key pressed
                        resp_keys.rt = _resp_keys_allKeys[0].rt
                        # a response ends the routine
                        continueRoutine = False
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in responseComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # -------Ending Routine "response"-------
            for thisComponent in responseComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            span_resp.addData('resp_stim.started', resp_stim.tStartRefresh)
            span_resp.addData('resp_stim.stopped', resp_stim.tStopRefresh)
            # check responses
            if resp_keys.keys in ['', [], None]:  # No response was made
                resp_keys.keys = None
            span_resp.addData('resp_keys.keys',resp_keys.keys)
            if resp_keys.keys != None:  # we had a response
                span_resp.addData('resp_keys.rt', resp_keys.rt)
            response_keys.append(resp_keys.keys[0])
            response_rt.append(resp_keys.rt)
            resp_stim.text = resp_keys.keys[0]
            # the Routine "response" was not non-slip safe, so reset the non-slip timer
            routineTimer.reset()
        # completed MAXLEVEL-level+1 repeats of 'span_resp'
        
        
        # ------Prepare to start Routine "response_last"-------
        continueRoutine = True
        routineTimer.add(1.000000)
        # update component parameters for each repeat
        resp_last.setText(resp_keys.keys[-1])
        # keep track of which components have finished
        response_lastComponents = [resp_last]
        for thisComponent in response_lastComponents:
            thisComponent.tStart = None
            thisComponent.tStop = None
            thisComponent.tStartRefresh = None
            thisComponent.tStopRefresh = None
            if hasattr(thisComponent, 'status'):
                thisComponent.status = NOT_STARTED
        # reset timers
        t = 0
        _timeToFirstFrame = win.getFutureFlipTime(clock="now")
        response_lastClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
        frameN = -1
        
        # -------Run Routine "response_last"-------
        while continueRoutine and routineTimer.getTime() > 0:
            # get current time
            t = response_lastClock.getTime()
            tThisFlip = win.getFutureFlipTime(clock=response_lastClock)
            tThisFlipGlobal = win.getFutureFlipTime(clock=None)
            frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
            # update/draw components on each frame
            
            # *resp_last* updates
            if resp_last.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                # keep track of start time/frame for later
                resp_last.frameNStart = frameN  # exact frame index
                resp_last.tStart = t  # local t and not account for scr refresh
                resp_last.tStartRefresh = tThisFlipGlobal  # on global time
                win.timeOnFlip(resp_last, 'tStartRefresh')  # time at next scr refresh
                resp_last.setAutoDraw(True)
            if resp_last.status == STARTED:
                # is it time to stop? (based on global clock, using actual start)
                if tThisFlipGlobal > resp_last.tStartRefresh + 1.0-frameTolerance:
                    # keep track of stop time/frame for later
                    resp_last.tStop = t  # not accounting for scr refresh
                    resp_last.frameNStop = frameN  # exact frame index
                    win.timeOnFlip(resp_last, 'tStopRefresh')  # time at next scr refresh
                    resp_last.setAutoDraw(False)
            
            # check for quit (typically the Esc key)
            if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                core.quit()
            
            # check if all components have finished
            if not continueRoutine:  # a component has requested a forced-end of Routine
                break
            continueRoutine = False  # will revert to True if at least one component still running
            for thisComponent in response_lastComponents:
                if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                    continueRoutine = True
                    break  # at least one component has not yet finished
            
            # refresh the screen
            if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                win.flip()
        
        # -------Ending Routine "response_last"-------
        for thisComponent in response_lastComponents:
            if hasattr(thisComponent, "setAutoDraw"):
                thisComponent.setAutoDraw(False)
        stair.addOtherData('resp_last.started', resp_last.tStartRefresh)
        stair.addOtherData('resp_last.stopped', resp_last.tStopRefresh)
        response_corr = [int(k) for k in response_keys] == list(np.flip(sequence))
        if response_corr:
            feedback_stim.text = 'Correct'
        else:
            feedback_stim.text = 'Wrong'
        
        stair.addResponse(response_corr)
        stair.addOtherData('resp_keys.keys', response_keys)
        stair.addOtherData('resp_keys.rt', response_rt)
        
        # set up handler to look after randomisation of conditions etc
        do_feedback = data.TrialHandler(nReps=int(expInfo['practice']), method='sequential', 
            extraInfo=expInfo, originPath=-1,
            trialList=[None],
            seed=None, name='do_feedback')
        thisExp.addLoop(do_feedback)  # add the loop to the experiment
        thisDo_feedback = do_feedback.trialList[0]  # so we can initialise stimuli with some values
        # abbreviate parameter names if possible (e.g. rgb = thisDo_feedback.rgb)
        if thisDo_feedback != None:
            for paramName in thisDo_feedback:
                exec('{} = thisDo_feedback[paramName]'.format(paramName))
        
        for thisDo_feedback in do_feedback:
            currentLoop = do_feedback
            # abbreviate parameter names if possible (e.g. rgb = thisDo_feedback.rgb)
            if thisDo_feedback != None:
                for paramName in thisDo_feedback:
                    exec('{} = thisDo_feedback[paramName]'.format(paramName))
            
            # ------Prepare to start Routine "feedback"-------
            continueRoutine = True
            routineTimer.add(1.000000)
            # update component parameters for each repeat
            # keep track of which components have finished
            feedbackComponents = [feedback_stim]
            for thisComponent in feedbackComponents:
                thisComponent.tStart = None
                thisComponent.tStop = None
                thisComponent.tStartRefresh = None
                thisComponent.tStopRefresh = None
                if hasattr(thisComponent, 'status'):
                    thisComponent.status = NOT_STARTED
            # reset timers
            t = 0
            _timeToFirstFrame = win.getFutureFlipTime(clock="now")
            feedbackClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
            frameN = -1
            
            # -------Run Routine "feedback"-------
            while continueRoutine and routineTimer.getTime() > 0:
                # get current time
                t = feedbackClock.getTime()
                tThisFlip = win.getFutureFlipTime(clock=feedbackClock)
                tThisFlipGlobal = win.getFutureFlipTime(clock=None)
                frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
                # update/draw components on each frame
                
                # *feedback_stim* updates
                if feedback_stim.status == NOT_STARTED and tThisFlip >= 0.0-frameTolerance:
                    # keep track of start time/frame for later
                    feedback_stim.frameNStart = frameN  # exact frame index
                    feedback_stim.tStart = t  # local t and not account for scr refresh
                    feedback_stim.tStartRefresh = tThisFlipGlobal  # on global time
                    win.timeOnFlip(feedback_stim, 'tStartRefresh')  # time at next scr refresh
                    feedback_stim.setAutoDraw(True)
                if feedback_stim.status == STARTED:
                    # is it time to stop? (based on global clock, using actual start)
                    if tThisFlipGlobal > feedback_stim.tStartRefresh + 1.0-frameTolerance:
                        # keep track of stop time/frame for later
                        feedback_stim.tStop = t  # not accounting for scr refresh
                        feedback_stim.frameNStop = frameN  # exact frame index
                        win.timeOnFlip(feedback_stim, 'tStopRefresh')  # time at next scr refresh
                        feedback_stim.setAutoDraw(False)
                
                # check for quit (typically the Esc key)
                if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
                    core.quit()
                
                # check if all components have finished
                if not continueRoutine:  # a component has requested a forced-end of Routine
                    break
                continueRoutine = False  # will revert to True if at least one component still running
                for thisComponent in feedbackComponents:
                    if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                        continueRoutine = True
                        break  # at least one component has not yet finished
                
                # refresh the screen
                if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
                    win.flip()
            
            # -------Ending Routine "feedback"-------
            for thisComponent in feedbackComponents:
                if hasattr(thisComponent, "setAutoDraw"):
                    thisComponent.setAutoDraw(False)
            do_feedback.addData('feedback_stim.started', feedback_stim.tStartRefresh)
            do_feedback.addData('feedback_stim.stopped', feedback_stim.tStopRefresh)
        # completed int(expInfo['practice']) repeats of 'do_feedback'
        
        thisExp.nextEntry()
        
    # staircase completed
    
    
    # ------Prepare to start Routine "stimulation_stop"-------
    continueRoutine = True
    # update component parameters for each repeat
    if not(expInfo['practice']):
        stimulator.stop()
    # keep track of which components have finished
    stimulation_stopComponents = []
    for thisComponent in stimulation_stopComponents:
        thisComponent.tStart = None
        thisComponent.tStop = None
        thisComponent.tStartRefresh = None
        thisComponent.tStopRefresh = None
        if hasattr(thisComponent, 'status'):
            thisComponent.status = NOT_STARTED
    # reset timers
    t = 0
    _timeToFirstFrame = win.getFutureFlipTime(clock="now")
    stimulation_stopClock.reset(-_timeToFirstFrame)  # t0 is time of first possible flip
    frameN = -1
    
    # -------Run Routine "stimulation_stop"-------
    while continueRoutine:
        # get current time
        t = stimulation_stopClock.getTime()
        tThisFlip = win.getFutureFlipTime(clock=stimulation_stopClock)
        tThisFlipGlobal = win.getFutureFlipTime(clock=None)
        frameN = frameN + 1  # number of completed frames (so 0 is the first frame)
        # update/draw components on each frame
        
        # check for quit (typically the Esc key)
        if endExpNow or defaultKeyboard.getKeys(keyList=["escape"]):
            core.quit()
        
        # check if all components have finished
        if not continueRoutine:  # a component has requested a forced-end of Routine
            break
        continueRoutine = False  # will revert to True if at least one component still running
        for thisComponent in stimulation_stopComponents:
            if hasattr(thisComponent, "status") and thisComponent.status != FINISHED:
                continueRoutine = True
                break  # at least one component has not yet finished
        
        # refresh the screen
        if continueRoutine:  # don't flip if this routine is over or we'll get a blank screen
            win.flip()
    
    # -------Ending Routine "stimulation_stop"-------
    for thisComponent in stimulation_stopComponents:
        if hasattr(thisComponent, "setAutoDraw"):
            thisComponent.setAutoDraw(False)
    # the Routine "stimulation_stop" was not non-slip safe, so reset the non-slip timer
    routineTimer.reset()
# completed 1 repeats of 'runs'

if not(expInfo['practice']):
    stimulator = None

# Flip one final time so any remaining win.callOnFlip() 
# and win.timeOnFlip() tasks get executed before quitting
win.flip()

# these shouldn't be strictly necessary (should auto-save)
thisExp.saveAsWideText(filename+'.csv')
thisExp.saveAsPickle(filename)
logging.flush()
# make sure everything is closed down
thisExp.abort()  # or data files will save again on exit
win.close()
core.quit()
